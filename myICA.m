function myICA(particSel, loadFlag)
% Written by Andrew Curran with input from Julia Rogge, 2018
% Personalized ICA component rejection within EEGlab based on Brainhack
% data obtained May 2018. 

if exist('loadFlag') % user wishes to identify file path themself
    selPath = uigetdir;
else
    selPath = 'BHCK\CleanedData';
end

fileList = dir([selPath '\*20181028.mat']);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

for ii = 1:numel(fileList)
    
load('BHCK\CleanedData\FiltCleanDatPartic2_20181028.mat')

% Make temp save to have valid EEG file
for jj = 1:length(myEEG)
    
EEG = myEEG(jj);

pop_saveset(EEG, [selPath '\temp']);
    
EEG = pop_loadset%('filename','temp.mat', 'filepath', selPath);


selFile = [fileList(particSel).folder '\' fileList(particSel).name];
[EEG(:).icaweights] = deal(icaweights);
[EEG(:).icasphere] = deal(icasphere);
EEG = eeg_checkset(EEG);
[EEG, com] = pop_selectcomps(EEG, [1:20]);



load(selFile,'allEEG','myEEG','icasphere','icaweights','mods');

[myEEG(:).icaweights] = deal(icaweights);
[myEEG(:).icasphere] = deal(icasphere);
EEG = myEEG(1);
EEG.icawinv = pinv(EEG.icaweights*EEG.icasphere);
EEG = eeg_checkset(EEG);
%[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
%[ALLEEG, EEG, CURRENTSET] = pop_newset( ALLEEG, EEG, 1 );
[EEG, com] = pop_selectcomps(EEG, [1:20]);
%[T, com] = pop_selectcomps(EEG);
% pop_prop
pause

% Nevermind this shenanigans. I had to change a bunch of code in the
% popups because APPARENTLY the makers of eeglab never considered that
% someone might want to evaluate ICA components by eye, outside of
% their stupid environment. Basically, the functions that are called
% for pop_selectcomps are modified to pass variables to the figures
% themselves and to the base, so this final line is the last step in a
% stupid, overly convoluted process
EEG = evalin('base','EEG');

CR=find(EEG.reject.gcompreject==1);

%save([selPath '\partic' num2str(particSel) '_ICAREJ_evalat' num2str(round(now*100000))],'CR')
end

clear myEEG icasphere icaweights mods allEEG
end

