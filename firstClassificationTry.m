%load('BHCK\CleanedData\ParticNum2_20181119.mat')

% Try isolating 10-20 hz band for classification first
clear p
clear T
cats = {ALLEEG(:).setname};
cats = cats';
maxItr = 14;
tic
for ii = 1:length(ALLEEG)
    
    EEG = ALLEEG(ii);
    
    data = EEG.data;
    
    for jj = 1:size(data,1)
        for kk = 1:maxItr
            a = 0.01*2^(kk-1);
            b = 0.01*2^(kk);
            p(ii,maxItr*(jj-1)+kk) = bandpower(data(jj,:),500,[a b]);
        end
        
    end
    
    
end
toc

T = array2table(p);
T.grp = cats;

%% Baseline vs Non
% Detecting baseline versus others. More stims than baseline, so baseline
% needs to be boosted first
% Using this code resulted in a best accuracy of 99.9! Fine tree (medium 
% tree also 97.4%, Fine KNN (although many KNNs performed well, 95%+,
% Boosted Trees 100%! Bagged Trees at 99%, Subspace KNN also 100%
desiredN = 1000;

bsMeasures = sum(contains(cats,'Baseline'));
baselineInds=find(contains(cats,'Baseline'));
justBaseline = p(baselineInds,:);
boostSample = randsample(bsMeasures,desiredN,true);
boostedBaseline = justBaseline(boostSample,:);

% Boost other stims too
stimMeasures = sum(~contains(cats,'Baseline'));
stimInds=find(~contains(cats,'Baseline'));
justStim = p(stimInds,:);
boostSample = randsample(stimMeasures,desiredN,true);
boostedStim = justStim(boostSample,:);

% concatenate
boostedCats = [repmat({'Base'},1000,1) ; repmat({'Stim'},1000,1)];
tBVS = array2table([boostedBaseline; boostedStim]);
tBVS.grps = boostedCats;

%% Baseline vs Stim vs Imagined
% In this case, best performers were Fine Tree (59.5%), most KNN (~60%),
% ensemble boosted/bagged trees and subspace KNN. Oddly though, KNN
% classifiers seemed to predict Real images more often than Imagined, while
% Tree methods shared a balanced 50/50. Baseline true positive rate was
% always 100% though across all!!!
desiredN = 1000;

bsMeasures = sum(contains(cats,'Baseline'));
baselineInds=find(contains(cats,'Baseline'));
justBaseline = p(baselineInds,:);
boostSample = randsample(bsMeasures,desiredN,true);
boostedBaseline = justBaseline(boostSample,:);

% Boost other stims too
realMeasures = sum(~contains(cats,'Real'));
realInds=find(~contains(cats,'Real'));
justReal = p(realInds,:);
boostSample = randsample(realMeasures,desiredN,true);
boostedReal = justReal(boostSample,:);

imajMeasures = sum(~contains(cats,'Imag'));
imajInds=find(~contains(cats,'Real'));
justImaj = p(imajInds,:);
boostSample = randsample(imajMeasures,desiredN,true);
boostedImaj = justImaj(boostSample,:);

% concatenate
boostedCats = [repmat({'Base'},1000,1) ; repmat({'Real'},1000,1) ; repmat({'Imag'},1000,1)];
tBVS = array2table([boostedBaseline; boostedReal; boostedImaj]);
tBVS.grps = boostedCats;

%% Removed extra lined
lines2remove = [5 52 60 64]; %ECG and eye lines

tempALLEEG = ALLEEG;

%ALLEEG(lines2remove) = [];

clear p
clear T
cats = {ALLEEG(:).setname};
cats = cats';
maxItr = 14;
tic
for ii = 1:length(ALLEEG)
    
    EEG = ALLEEG(ii);
    
    data = EEG.data;
    data(lines2remove,:) = [];
    
    for jj = 1:size(data,1)
        for kk = 1:maxItr
            a = 0.01*2^(kk-1);
            b = 0.01*2^(kk);
            p(ii,maxItr*(jj-1)+kk) = bandpower(data(jj,:),500,[a b]);
        end
        
    end
    
    
end
toc

T = array2table(p);
T.grp = cats;

desiredN = 1000;

bsMeasures = sum(contains(cats,'Baseline'));
baselineInds=find(contains(cats,'Baseline'));
justBaseline = p(baselineInds,:);
boostSample = randsample(bsMeasures,desiredN,true);
boostedBaseline = justBaseline(boostSample,:);

% Boost other stims too
realMeasures = sum(~contains(cats,'Real'));
realInds=find(~contains(cats,'Real'));
justReal = p(realInds,:);
boostSample = randsample(realMeasures,desiredN,true);
boostedReal = justReal(boostSample,:);

imajMeasures = sum(~contains(cats,'Imag'));
imajInds=find(~contains(cats,'Real'));
justImaj = p(imajInds,:);
boostSample = randsample(imajMeasures,desiredN,true);
boostedImaj = justImaj(boostSample,:);

% concatenate
boostedCats = [repmat({'Base'},1000,1) ; repmat({'Real'},1000,1) ; repmat({'Imag'},1000,1)];
tBVS = array2table([boostedBaseline; boostedReal; boostedImaj]);
tBVS.grps = boostedCats;


%% Real Comparisons
% After fixing to remove some sets from the existing set, it turns out the
% model performs poorly against new data, even when it reaches 100%
% classification. Lame!
lines2remove = [5 52 60 64]; %ECG and eye lines

tempALLEEG = ALLEEG;

clear p
clear T
cats = {ALLEEG(:).setname};
cats = cats';
maxItr = 14;
tic
for ii = 1:length(ALLEEG)
    
    EEG = ALLEEG(ii);
    
    data = EEG.data;
    data(lines2remove,:) = [];
    
    for jj = 1:size(data,1)
        for kk = 1:maxItr
            a = 0.01*2^(kk-1);
            b = 0.01*2^(kk);
            p(ii,maxItr*(jj-1)+kk) = bandpower(data(jj,:),500,[a b]);
        end
        
    end
    
    
end
toc

desiredN = 18;
cats = {ALLEEG(:).setname};
cats(find(contains(cats,'Imag')))=[];
uniCats = unique(cats);
boostHold = [];
catHold = [];
heldBack = [];
backCat = [];
for ii = 1:length(uniCats)
    measures = sum(contains(cats,uniCats{ii}));
    inds = find(contains(cats,uniCats{ii}));
    justMeas = p(inds,:);
    % Hold some sets back for testing later
    %heldMeas = justMeas(end-3:end,:);
    %justMeas(end-3:end,:) = [];
    %boost = randsample(measures-4,desiredN,true);
    boost = randsample(measures,desiredN,true);
    boostMeas = justMeas(boost,:);
    boostCat = repmat(uniCats(ii),desiredN,1);
    boostHold = [boostHold; boostMeas];
    catHold = [catHold; boostCat];
    %heldBack = [heldBack; heldMeas];
    %backCat = [backCat; repmat(uniCats(ii),4,1)];
end

tBVS = array2table(boostHold );
tBVS.grps = catHold;

heldBVS = array2table(heldBack);
heldBVS.grp = backCat;



