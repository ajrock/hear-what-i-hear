close all
clearvars
sca
global cogent;           
%% %%%%%%%%%%%%%%%%%%
WavFile = {...
    'chirps.wav';...
    'BitDesolation.wav';...
    'Babble.wav';...
    'A4Sine.wav';...
    'AMWN.wav';...
    %'noiseburst50_000.wav';...
    };
nrep = 1;
isi = [.5];
stimDur = 1;
imgDur = 15;
filePath = ('C:\Users\neuro\Desktop\BHCK\Runs');
dur_task = stimDur+imgDur;
sF = 44100; % Sampling Frequency

dB = 0;

nstim = length(WavFile(:));%exclude noise burst for now
StimList = repmat([1:nstim],[1,nrep]);
StimList = StimList(randperm(length(StimList(:))))

%% %%%%%%%%%%%%%%%%%init
addpath(['C:\Users\neuro\Desktop\BHCK\trigger']);
addpath(['C:\Users\neuro\Desktop\BHCK']);

%%%%initilize trigger
config_io;
address = hex2dec('D010');
InitializePsychSound();
PsychDefaultSetup(1);

save([filePath '\RunAt_' datestr(now,'yy-mm-dd_HH-MM')], 'StimList', 'WavFile')

ntrial = length(StimList(:));
t0 = GetSecs;
for itr = 1:ntrial

    is = StimList(itr);
    disp(sprintf('Trial: %d, Wave File: %s',itr,WavFile{is}));

    %%%%%%init Sound
    [wav, freq] = wavread(WavFile{is});
    wav = wav'*10^(dB/20); % resize for output desired
    wav = wav(:,1:sF*stimDur); % restrict to desired length
    % Add tiny fadeout
    wav(end-119:end) = wav(end-119:end).*linspace(1,0,120);
    nch = 1;%size(wav,1); % Number of rows == number of channels.

    if itr == 1
       haudio = PsychPortAudio('Open', [], [], 0, freq, nch);
    end
    
    PsychPortAudio('FillBuffer', haudio, wav(1,:));
    
    dt = GetSecs-t0;
    if isi-dt>0
        WaitSecs(isi-dt);
    end
     
    %PRESENT IMG
    snd = PsychPortAudio('Start', haudio, [], 0, 1);
   io64(cogent.io.ioObj,address,is);WaitSecs(0.005);
   io64(cogent.io.ioObj,address,0);                       
    WaitSecs(dur_task);

    beep
    
    %Button Press
    buttons = 0;
    while buttons == 0;        
        [x,y,buttons] = GetMouse;
    end
    t0 = GetSecs;
    WaitSecs(2);
    
end
