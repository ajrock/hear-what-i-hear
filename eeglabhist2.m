% EEGLAB history file generated on the 22-Oct-2018
% ------------------------------------------------
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
EEG = pop_loadbv('BHCK\EEGData\', 'XIP06-1.vhdr', [1 89870], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64]);
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'setname','raw1','gui','off'); 
EEG = pop_loadbv('BHCK\EEGData\', 'XIP06-2.vhdr', [1 88850], [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64]);
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'setname','raw2','gui','off'); 
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'retrieve',[1 2] ,'study',0); 
pop_saveh( ALLCOM, 'eeglabhist2.m', 'BHCK\');
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, [1 2] ,'retrieve',1,'study',0); 
EEG=pop_chanedit(EEG, 'lookup','eeglab14_1_1b\\plugins\\dipfit2.3\\standard_BESA\\standard-10-5-cap385.elp');
[ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
pop_saveh( ALLCOM, 'eeglabhist2.m', 'BHCK\');
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'retrieve',[1 2] ,'study',0); 
EEG = eeg_checkset( EEG );
pop_eegfilt = EEG( pop_eegfilt,EEG);0.1,0,[],0 = 
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, [1 2] ,'study',0); 
pop_saveh( ALLCOM, 'eeglabhist2.m', 'BHCK\');
eeglab redraw;
