%% Evaluate this section once to get file names of all datasets in the current folder (check List to avoid mistakes manually!)


%function betadots_ICA_helper2(datas, datan)


% datas: starting dataset to look at 
% datan: final dataset to look at

LP = '/Users/user/Documents/RDM/BETA_DOTS/EEG_Experiment/EEG/post_amica/'; 

filez=dir([LP '*.set']);
for c = 1:size(filez,1)
 
    disp(filez(c).name)
end

%mkdir('/Users/user/Documents/RDM/BETA_DOTS/EEG_Experiment/EEG/', 'New_Ica_Removed');

%SPATH='/Users/user/Documents/RDM/BETA_DOTS/EEG_Experiment/EEG/N';

%savepath='/Volumes/AGF work/SMAC/1auswertung/3flanker/eeg/ICA_auswertung/error_discr_0_120_ms/';

%bc=pwd;cd(savepath);
%load ([savepath 'compindex.mat'])
%save(['Error from ' num2str(EEG.times(Ftest_window(1))) ' to ' num2str(EEG.times(Ftest_window(end))) '.mat'],'allcomp','allPRED','allSLOW','allACT','allEGG');
%load(['Error from 0 to 110.mat']);
%cd(bc)


%%
%This section opens the remove components window and either also plots all
%components (set PLOTALL = 1) or leaves this open.


% open single component and press accept (--> marks component as rejected) 


%After a pause, components marked for removal will be removed and the
%dataset will be saved under the original plus '_comp_removed' as a suffix.
%Additionally, a matrix that saves all removed components for later use is
%generated.



for c= 23%c =1 : length(filez)     
    
    %: size(filez,1)
    clearvars -except filez c compindex allcomp LP
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    
    EEG = pop_loadset('filename', filez(c).name, 'filepath', LP); % added filepath
    load(['/Users/user/Documents/RDM/BETA_DOTS/EEG_Experiment/BEH_INFO/'  EEG.setname(1:5) '.mat'])
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    %[T, com] = pop_selectcomps(EEG, [1:10 setdiff([allcomp{c}(:,1)],[1:10])]); %
    [T, com] = pop_selectcomps(EEG, [1:20]); %change to maximum amount of electrodes
    %pop_eegplot( EEG, 0, 1, 0)
    %eegplot( EEG.data, 'dispchans', 5, 'winlength', 2); %show ICA components in continuous data
    
    pause
    
    CR=find(EEG.reject.gcompreject==1);
    %EEG = pop_subcomp(EEG, CR, 0);
    %[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off'); 
    EEG = eeg_checkset(EEG);
    %EEG = pop_editset(EEG, 'setname', [EEG.setname(1:7) 'ICA Comp Removed']);
    [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    
    %pop_saveset(EEG,  [savepath EEG.setname(1:7) 'ICA Comp Removed.set'], SPATH);
    EEG = pop_saveset(EEG, 'filename', [LP filez(c).name]); %EEG.setname(1:7) 'ICA Comp Removed'
    ALLEEG = pop_delset(ALLEEG, [1  2]);
    close all
    
    %Store removed components in BEH file 
    VP_INFO.ICA_Remove2 = CR;
    save(['/Users/user/Documents/RDM/BETA_DOTS/EEG_Experiment/BEH_INFO/' VP_INFO.name], 'VP_INFO')
    
end

%

