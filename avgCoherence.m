% Checking average coherene for tone based stimulation of participant six
% across all trials
tic
clear
eegPath = 'BHCK\CleanedData\';
eegDatName = 'EpochedData280918.mat';
load([eegPath eegDatName])

fs = 44100; %Sample rate of wav files

fsDat = 500;

% Load just 6th participant
% Reminder of struct form: 3D matrix of structures of EEG data, of MxNxO
% data: M = participant #, N = Trial number, O = stimcuts:
% 1 = baseline
% 2:6 = stimulated runs
% 7:11 = imagined runs
% 2+7 = chirps
% 3+8 = BitDesolation
% 4+9 = Cafe Babble
% 5+10 = 440 Hz sinewav (A4)
% 6+11 = amplitude modulated white noise
% Only five seconds
eegDat = squeeze(epochEEG(6,:,:));
clear epochEEG

toneDat = squeeze(eegDat(:,5));
baseline = squeeze(eegDat(:,1));
imDat = squeeze(eegDat(:,10));
clear eegDat d

% In this data, iteration 19 seems incomplete
toneDat(19,:)=[];

% Looking at coherence, a signal processing metric that searches for
% similar levels of presence of frequencies across two signals. For now,
% doing pair-wise comparisons across trials, but a better method may be to
% average the freq response, use ifft to reverse, and calculate coherence
% in comparison with this reference average signal. Done each for
% stimulated, baseline, and imagined stims
toc
toneCoh = zeros(18,64,513);
imCoh = zeros(18,64,513);
baseCoh = zeros(18,64,513);
for ii = 1:18
    for jj = 1:64
        if ii == 18
            [toneCoh(ii,jj,1:513),freqDat] = Mscohere(toneDat(ii).data(jj,:),toneDat(1).data(jj,:),[],[],[],500);
            [imCoh(ii,jj,1:513),freqDat] = Mscohere(imDat(ii).data(jj,:),imDat(1).data(jj,:),[],[],[],500);
            [baseCoh(ii,jj,1:513),freqDat] = Mscohere(baseline(ii).data(jj,:),baseline(1).data(jj,:),[],[],[],500);
        else
            toneCoh(ii,jj,1:513) = Mscohere(toneDat(ii).data(jj,:),toneDat(ii+1).data(jj,:),[],[],[],500);
            imCoh(ii,jj,1:513) = Mscohere(imDat(ii).data(jj,:),imDat(ii+1).data(jj,:),[],[],[],500);
            baseCoh(ii,jj,1:513) = Mscohere(baseline(ii).data(jj,:),baseline(ii+1).data(jj,:),[],[],[],500);
        end    
    end
end

% Keep in mind that to reverse a fft transform, use ifft

% Since there is so much data to search, I will just take the average
% coherence for each channel. I can then use a total coherence for each
% channel and see which one a) maximizes stim and im coherence while b)
% minimizing all baseline coherence, hopefully resulting in something
% useful...
totCoh = [];
for ii = 1:64
    [toneVsIm]   =Mscohere(squeeze(mean(toneCoh(:,ii,:),1)),squeeze(mean(imCoh(:,ii,:),1)));
    [toneVsBase] =Mscohere(squeeze(mean(toneCoh(:,ii,:),1)),squeeze(mean(baseCoh(:,ii,:),1)));
    [imVsBase]   =Mscohere(squeeze(mean(imCoh(:,ii,:),1)),squeeze(mean(baseCoh(:,ii,:),1)));
    totCoh = [totCoh; sum([toneVsIm toneVsBase imVsBase])];
end

optiCoh = totCoh(:,1)-(totCoh(:,2) + totCoh(:,3))/2;
[sortedCoh, id] = sort(optiCoh);

% Take... best three channels? Or best two and check their coherence as
% well?
channels2keep = id(end-1:end);

