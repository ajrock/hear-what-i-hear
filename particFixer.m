% Adds stimDur values to mat workspaces from BrainHack 2018 that were
% missing from original. Participants 2-4 need fixing. In general, the
% first 9 runs are always 10 seconds Real stimuli and all following are 1
% s.
clear
filePath = 'BHCK\CleanedData\';

particList = {'Participant 2', 'Participant 3', 'Participant 4'};

saveVarList = {'WavFile','StimList','stimDur'};

for ii = 1:length(particList)
    d = dir([filePath particList{ii} '\*.mat']);
    for jj = 1:length(d)
        load([d(jj).folder '\' d(jj).name])
        % Separate run number from name
        if str2num(d(1).name(4:strfind(d(1).name,'Part')-1))<10 % too lazy to do properly
            stimDur = 10;
        else
            stimDur = 1;
        end
        save([d(jj).folder '\' d(jj).name],...
            saveVarList{1},...
            saveVarList{2},...
            saveVarList{3});
    end
end
