clear
close all
load('BHCK\thisCantBeRight.mat')
lines2remove = [5 52 60 64]; 
pcaCo1= reshape(trainedModel.PCACoefficients(:,1),14,60);
pcaCo1 = pcaCo1(4,:);
pcaCo2= reshape(trainedModel.PCACoefficients(:,2),14,60);
pcaCo2 = pcaCo2(4,:);
pcaCo3= reshape(trainedModel.PCACoefficients(:,3),14,60);
pcaCo3 = pcaCo3(4,:);
pcaCo4= reshape(trainedModel.PCACoefficients(:,4),14,60);
pcaCo4 = pcaCo4(4,:);
pcaCo5= reshape(trainedModel.PCACoefficients(:,5),14,60);
pcaCo5 = pcaCo5(4,:);
pcaCo6= reshape(trainedModel.PCACoefficients(:,6),14,60);
pcaCo6 = pcaCo6(4,:);
pcaCo7= reshape(trainedModel.PCACoefficients(:,7),14,60);
pcaCo7 = pcaCo7(4,:);
xlab = {EEG.chanlocs.labels};
xlab(lines2remove)=[];
vect = [0.01*2.^([1:14]-1); 0.01*2.^([1:14])];
ylab = cellstr(strcat(num2str(vect(1,:)'), ' - ',num2str(vect(2,:)')));

figure; plot([pcaCo1;pcaCo2;pcaCo3;pcaCo4;pcaCo5;pcaCo6;pcaCo7])

figure; surf(pcaCo1)
title('1 PCA Component')
xticks([1:64]);
xticklabels(xlab);
xtickangle(45);
% yticks([1:14]);
% yticklabels(ylab);
view(2)

figure; surf(pcaCo2)
title('2 PCA Component')
xticks([1:64]);
xticklabels(xlab);
xtickangle(45);
% yticks([1:14]);
% yticklabels(ylab);
view(2)

figure; surf(pcaCo3)
title('3 PCA Component')
xticks([1:64]);
xticklabels(xlab);
xtickangle(45);
% yticks([1:14]);
% yticklabels(ylab);
view(2)

figure; surf(pcaCo4)
title('4 PCA Component')
xticks([1:64]);
xticklabels(xlab);
xtickangle(45);
% yticks([1:14]);
% yticklabels(ylab);
view(2)

figure; surf(pcaCo5)
title('5 PCA Component')
xticks([1:64]);
xticklabels(xlab);
xtickangle(45);
% yticks([1:14]);
% yticklabels(ylab);
view(2)

figure; surf(pcaCo6)
title('6 PCA Component')
xticks([1:64]);
xticklabels(xlab);
xtickangle(45);
% yticks([1:14]);
% yticklabels(ylab);
view(2)

figure; surf(pcaCo7)
title('7 PCA Component')
xticks([1:64]);
xticklabels(xlab);
xtickangle(45);
% yticks([1:14]);
% yticklabels(ylab);
view(2)
