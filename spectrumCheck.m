% Spectrum check of wav data from BHCK experiment
close all

clear
wavPath = 'BHCK\Stimulations\';
eegPath = 'BHCK\CleanedData\';
eegDatName = 'EpochedData280918.mat';
load([eegPath eegDatName])

d = dir([wavPath '*.wav']);
fs = 44100; % sampling rate used; normal for sound processing

for ii = 1:length(d)
    thisWav = d(ii).name;
    thisFile = audioread([wavPath thisWav]);
    
    stims(ii).wavName = thisWav;
    stims(ii).signal = thisFile(:,1); %Two channels, but same in each
    stims(ii).fft = fft_f(stims(ii).signal,fs);
    %figure;
    %sigLength = length(stims(ii).signal)/fs;
    %t = 1/fs:1/fs:sigLength;
    %plot(t,stims(ii).signal)
    %fft_f(stims(ii).signal,fs);
    %title(thisWav)
    
end

% Load just 6th participant
% Reminder of struct form: 3D matrix of structures of EEG data, of MxNxO
% data: M = participant #, N = Trial number, O = stimcuts:
% 1 = baseline
% 2:6 = stimulated runs
% 7:11 = imagined runs
% 2+7 = chirps
% 3+8 = BitDesolation
% 4+9 = Cafe Babble
% 5+10 = 440 Hz sinewav (A4)
% 6+11 = amplitude modulated white noise
% Only five seconds
eegDat = squeeze(epochEEG(6,:,:));
clear epochEEG

toneDat = squeeze(eegDat(:,5));
clear eegDat
% In this data, iteration 19 seems incomplete
toneDat(19,:)=[];


oneTrial = toneDat(2).data;
twoTrial = toneDat(3).data;
specSig = zeros(64,1251);
specSig2 = zeros(64,1251);
maxNormSpecSig = zeros(64,1251);
maxNormSpecSig2 = zeros(64,1251);
freqDat = zeros(1,1251);

clear toneDat stims thisFile

for ii = 1:64
    [thisSpecSig,freqDat(1,:)] = fft_f(oneTrial(ii,:),500);
    maxNormSpecSig(ii,:) = abs(thisSpecSig)/max(abs(thisSpecSig));
    specSig(ii,:) = abs(thisSpecSig);
    [thisSpecSig,~] = fft_f(twoTrial(ii,:),500);
    maxNormSpecSig2(ii,:) = abs(thisSpecSig)/max(abs(thisSpecSig));    
    specSig2(ii,:) = abs(thisSpecSig);
end

diffData = specSig2 - specSig;
congData = (specSig2 + specSig)/2;


%% Fig explorer
% for ii = 1:64
%     figure; semilogx(freqDat(1,:),specSig(ii,:))
%     title(toneDat(1).chanlocs(ii).labels)
%     movegui('northwest')
%     figure; semilogx(freqDat(1,:),specSig2(ii,:))
%     title(toneDat(1).chanlocs(ii).labels)
%     movegui('northeast')
%     figure; semilogx(freqDat(1,:),abs(diffData(ii,:)))
%     title(toneDat(1).chanlocs(ii).labels)
%     movegui('northwest')
%     figure; semilogx(freqDat(1,:),abs(congData(ii,:)))
%     title(toneDat(1).chanlocs(ii).labels)
%     movegui('northeast')
% %     xlim([25,250])
% %     hold on
% %     line([220 220],[0 0.5],'Color','r')
% %     line([110 110],[0 0.5],'Color','r')
% %     line([55 55],[0 0.5],'Color','r')
% %     hold off
%     dummy = input('Cont','s');
%     close all
% end

%% Congruence
% Trying something else. I want to see how much congruence I get between
% two trials across the same channel. That means that more or less, I need
% to set the max at each tested frequency 
normSig1 = zeros(64,1251);
normSig2 = normSig1;
congruence = normSig1;
for ii = 1:64
    normVals = max([specSig(ii,:); specSig2(ii,:)]);
    maxNormVals = max([maxNormSpecSig(ii,:); maxNormSpecSig2(ii,:)]);
    normSig1(ii,:) = specSig(ii,:)./normVals;
    maxNormSig1(ii,:) = maxNormSpecSig(ii,:)./maxNormVals;
    maxNormSig2(ii,:) = maxNormSpecSig2(ii,:)./maxNormVals;
    normSig2(ii,:) = specSig2(ii,:)./normVals;
    %congruence(ii,:) = (normSig2(ii,:) + normSig1)/2;
    %avgCong(ii) = mean(congruence(ii,:));
end
congruence = (normSig2 + normSig1)/2;
avgCong = mean(congruence,2);

maxNormCong = (maxNormSig1 +maxNormSig2)/2;
maxNormAvgCong = mean(maxNormCong,2);

% Visualize congruence levels. Currently, only every 10 freqs is
% visualized, so it might help to make sub averages across these, OR select
% specific freq bands!
figure; surf(1:64,freqDat(1:10:end),plotCong(1:10:end,:));view(2);
figure; surf(1:64,freqDat(freqDat>190&freqDat<200),plotCong(freqDat>190&freqDat<200,:));view(2);

% I mean, what I need, soon, is to check this congruence measure against
% baseline and imaginary stimulus, otherwise I just have no idea.
