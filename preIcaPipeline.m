% Commands to carry out to perform ICA
% Follows instructions from https://sccn.ucsd.edu/wiki/Makoto's_preprocessing_pipeline
% Some instructions taken from Epoching script found in same folder
clear
%myEEG.etc.eeglabvers = '14.1.1'; % this tracks which version of EEGLAB is being used, you may ignore it
% Needed to load channels from BV load
chanList = 1:1:64;
% General file path, replace with your path!
filePath = 'BHCK\CleanedData\';
load([filePath 'fields'])


% Loop information. Each participant and each trial for each participant.
% Skip 1 for now, poorly made trials
for participantNumber = 2:6

thisParticipant = sprintf('Participant %d',participantNumber);
% Get a count of how many Trials were performed
d = dir([filePath thisParticipant '\*.eeg']);

% Loop over trials
for itr = 1:min([length(d) 18])

% Load each trial
thisHeader = sprintf('XIP%02d-%d.vhdr',participantNumber,itr);
myEEG(itr) = pop_loadbv([filePath thisParticipant '\'], thisHeader, [], chanList);
myEEG(itr).setname='preICA';
% Check data
myEEG(itr) = eeg_checkset( myEEG(participantNumber-1) );

% Locate Channels
%Insert your own channel locations here!
myEEG(itr) = pop_chanedit(myEEG(itr), 'lookup','eeglab14_1_1b\\plugins\\dipfit2.3\\standard_BESA\\standard-10-5-cap385.elp');

% High pass at 0.1 Hz
myEEG(itr) = pop_eegfilt(myEEG(itr), 0.1, 0, []);

% Clean line noise
myEEG(itr) = pop_cleanline(myEEG(itr), 'Bandwidth',2,'ChanCompIndices',[1:myEEG(itr).nbchan], ...
    'SignalType','Channels','ComputeSpectralPower',true,             ...
    'LineFrequencies',[50 100] ,'NormalizeSpectrum',false,           ...
    'LineAlpha',0.01,'PaddingFactor',2,'PlotFigures',false,          ...
    'ScanForLines',true,'SmoothingFactor',100,'VerboseOutput',0,    ...
    'SlidingWinLength',myEEG(itr).pnts/myEEG(itr).srate,'SlidingWinStep',myEEG(itr).pnts/myEEG(itr).srate);

end

% Need to decide ahead of time whether to run one trial per participant and
% use those weights for all, or run all trials together
% reply = input('One trial or all? 1 = all','s');

% if str2num(reply) == 1
    % Treat each channel as long continuous channels
    allEEG = [myEEG(:).data];
    [icaweights, icasphere, mods] = runamica15(allEEG(:,:));
%     
% else
%     [icaweights, icasphere, mods] = runamica15( myEEG(1).data(:,:));
% end

% Saves workspace to individual participant mat files. Might be a bit
% overkill, but after doing so much work, I'd hate for interruptions to
% cause us to do that over again.
save([filePath sprintf('FiltCleanDatPartic%d',participantNumber) datestr(floor(now),'yyyymmdd')])

% This should get us to the point where we can at least look at the ICAs
% afterwards, once I lift the script from EEGLAB

end

