close all
clearvars
sca

%% %%%%%%%%%%%%%%%%%%
WavFile = {...
    'noiseburst50_000.wav';...
    };

StimList = [1 1 1 1 1 1];

dur_task = 1;
isi = [.5];

dB = 0;


%% %%%%%%%%%%%%%%%%%init
addpath(['C:\_User\Ketamin\Skripte\trigger']);
addpath(['C:\_User\Ketamin\Skripte\stimuli\startle']);

%%%%initilize trigger
try
    my_triggerhandler = triggerhandler();
    my_triggerhandler.send_trigger(99);
catch 
    disp(['KANN TRIGGER NICHT SENDEN - MSG:' error.message]);
    return;
end;
InitializePsychSound();
PsychDefaultSetup(1);


ntrial = length(StimList(:));
t0 = GetSecs;
for itr = 1:ntrial

    is = StimList(itr);
    disp(sprintf('Trial: %d, Wave File: %s',itr,WavFile{is}));

    %%%%%%init Sound
    [wav, freq] = psychwavread(WavFile{is});
    wav = wav'*10^(dB/20);
    nch = size(wav,1); % Number of rows == number of channels.

    if itr == 1
       haudio = PsychPortAudio('Open', [], [], 0, freq, nch);
    end
    
    PsychPortAudio('FillBuffer', haudio, wav);
    
    dt = GetSecs-t0;
    if isi-dt>0
        WaitSecs(isi-dt);
    end
     
    %PRESENT IMG
    snd = PsychPortAudio('Start', haudio, [], 0, 1);
    my_triggerhandler.send_trigger(is);
    WaitSecs(dur_task);

    %Button Press
    buttons = 0;
    while buttons == 0;        
        [x,y,buttons] = GetMouse;
    end
    t0 = GetSecs;
    
end
