function showAllICA()
% Written by Andrew Curran with input from Julia Rogge, 2018
% Personalized ICA component rejection within EEGlab based on Brainhack
% data obtained May 2018. 


selPath = 'BHCK\CleanedData';

fileList = dir([selPath '\*20181028.mat']);

for ii = 1:5

selFile = [fileList(ii).folder '\' fileList(ii).name];

load(selFile,'allEEG','myEEG','icasphere','icaweights','mods');

[myEEG(:).icaweights] = deal(icaweights);
[myEEG(:).icasphere] = deal(icasphere);
EEG = myEEG(1);
EEG.icawinv = pinv(EEG.icaweights*EEG.icasphere);
EEG = eeg_checkset(EEG);

[~, ~] = pop_selectcomps(EEG, [1:20]);

end



