% EEGLAB history file generated on the 18-Sep-2018
% Modified same day by Andrew Curran
% Major Mod 2018-10-22 Andrew Curran: adding in ICA 
% ------------------------------------------------
clear
close all
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
EEG.etc.eeglabvers = '14.1.1'; % this tracks which version of EEGLAB is being used, you may ignore it
% Needed to load channels from BV load
chanList = 1:1:64;
% General file path
filePath = 'BHCK\CleanedData\';
load([filePath 'fields'])

% Change this number to control how long after Real stim ends to take
% imagined stimulus
offset = 0.5; % Intuitive pick (aka more or less random)
imBoundaries = [-1 10.01]; % This is the boundary around each stim that will be taken

% Loop information. Each participant and each trial for each participant.
% Skip 1 for now, poorly made trials
for participantNumber = 2

thisParticipant = sprintf('Participant %d',participantNumber);
% Get a count of how many Trials were performed
d = dir([filePath thisParticipant '\*.eeg']);

% Loop over trials
for itr = 1:min([length(d) 18])

thisHeader = sprintf('XIP%02d-%d.vhdr',participantNumber,itr);

% Assume that first 9 trials are long, last 9 are short
if itr > 9
    boundaries = [0.01 10.01];
    group = 'Short stim';
else
    boundaries = [0.01 1.01];
    group = 'Long stim';
end

% Retrive Real Stimulus length information
thisMatFile = sprintf('\\Run%02dParticipant%d',itr,participantNumber);
% sometimes file has leading 0, sometimes not
if exist([filePath thisParticipant thisMatFile '.mat'],'file')
    load([filePath thisParticipant thisMatFile])
else
    thisMatFile = sprintf('\\Run%dParticipant%d',itr,participantNumber);
    load([filePath thisParticipant thisMatFile])
end
loadedEEG = pop_loadbv([filePath thisParticipant '\'], thisHeader, [], chanList);
loadedEEG.setname='firstTry';
loadedEEG = eeg_checkset( loadedEEG );
%EEG = eeg_checkset( EEG );
%EEG = eeg_checkset( EEG ); %I think these extra two are superfluous but I
%dont reaaaally know
% We will repeat this at least 11 times per participant; For starters, we
% will take 1 second each of real and imagined stims (remember that this is
% based off the mat files (stim dur is important for locating imagined
% portion) and 1 second of baseline (needs special case for extraction, ok just figured time does not start at exactly 0)
% pop_epoch inputs; EEG struct, cell of char vectors (we want separate
% structures so we will run this several times for each type)
% Baseline
epochEEG = pop_epoch( loadedEEG, {'boundary'}, [0.01 10.01], 'newname', 'Baseline', 'epochinfo', 'yes');
epochEEG.subject = ['Subject ' num2str(participantNumber)];
epochEEG.condition = 'Baseline';
epochEEG.session = num2str(itr);
epochEEG.group = group;
epochEEG = pop_rmbase( epochEEG,[]);
[ALLEEG, EEG, ind] = eeg_store(ALLEEG, epochEEG);

%epochEEG(participantNumber,itr,1) = pop_epoch( EEG, {'boundary'}, [0.01 10.01], 'newname', 'Baseline', 'epochinfo', 'yes');
%epochEEG(participantNumber,itr,1) = pop_rmbase( epochEEG(participantNumber,itr,1),[]);
for ii = 2:6 % Real stims
    thisName = WavFile{ii-1}; % Named for stim type
    epochEEG = pop_epoch( loadedEEG, {sprintf('S  %d',ii-1)}, boundaries, 'newname', ['Real' thisName], 'epochinfo', 'yes');
    epochEEG.subject = ['Subject ' num2str(participantNumber)];
    epochEEG.condition = sprintf('S  %d',ii-1);
    epochEEG.session = num2str(itr);
    epochEEG.group = group;
    epochEEG = pop_rmbase( epochEEG,[]);
    [ALLEEG, EEG, ind] = eeg_store(ALLEEG, epochEEG);
end
for ii = 7:11 % Imagined stims. Always uses longer boundaries
    thisName = WavFile{ii-6}; % Named for stim type
    epochEEG = pop_epoch( loadedEEG, {sprintf('S  %d',ii-6)}, stimDur+offset+imBoundaries, 'newname', ['Imag' thisName], 'epochinfo', 'yes');
    epochEEG.subject = ['Subject ' num2str(participantNumber)];
    epochEEG.condition = sprintf('S  %d',ii-1);
    epochEEG.session = num2str(itr);
    epochEEG.group = group;
    epochEEG= pop_rmbase( epochEEG,[]);
    [ALLEEG, EEG, ind] = eeg_store(ALLEEG, epochEEG);
end

end
% Run ICA
data = [ALLEEG(:).data];
[weights,sphere] = runica([ALLEEG(:).data]);
[ALLEEG(:).icaweights] = deal(weights);
[ALLEEG(:).icasphere] = deal(sphere);
[ALLEEG(:).icachansind] = deal(EEG.icachansind);
EEG = pop_chanedit(EEG, 'lookup','C:\\Matlab Drive\\Helpful Scripts\\eeglab14_1_1b\\functions\\resources\\Standard-10-5-Cap385_witheog.elp');
[ALLEEG(:).chanlocs] = deal(EEG.chanlocs);
[ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
[EEG ALLEEG CURRENTSET] = eeg_retrieve(ALLEEG,1);
[EEG, com] = pop_selectcomps(EEG, [1:30]); % Remember to try to open files in eeglab before allowing this line to run
storedEEG = EEG;
% Remove rejected components
component_keep = setdiff_bc(1:size(EEG.icaweights,1), find(EEG.reject.gcompreject));
for jj = 1:length(ALLEEG)
EEG = ALLEEG(jj);
compproj = EEG.icawinv(:, component_keep)*eeg_getdatact(EEG, 'component', component_keep, 'reshape', '2d');
compproj = reshape(compproj, size(compproj,1), EEG.pnts, EEG.trials);
ALLEEG(jj).reject.gcompreject = EEG.reject.gcompreject;
ALLEEG(jj).data = compproj;
end
save([filePath 'ParticNum' num2str(participantNumber) '_' datestr(floor(now),'yyyymmdd')],'ALLEEG')
ALLEEG = [];
end
%save([filePath 'EpochedData' datestr(floor(now),'yyyymmdd')])
% Extra visualization code was removed, should be easy to figure out.









