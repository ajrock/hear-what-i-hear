clear
file = 'XIP06-1';
format = '.eeg';
folder = 'BHCK\CleanedData\Participant 6\';

% % Use this code to extract event types from header info
% temp = [];
% temp.trialdef.eventtype = '?';
% temp.trialfun = 'ft_trialfun_general';
% temp.dataset = [folder file format];
% temp = ft_definetrial(temp);
% 
% % normal processing
% cfg = [];
% cfg.event = temp.event; %use events gotten from general trial fun
% % Assign baseline value to new segment start
% cfg.event(1).value = 'S  0';
% cfg.trialdef.eventtype = {temp.event.type}';
% cfg.trialdef.eventvalue{1} = 'S  0';
% %cfg.trialdef.eventtype = 'gui';
% cfg.trialdef.prestim = 0;
% cfg.trialdef.poststim = 0;
% cfg.trialdef.triallength = 10;
% cfg.trialfun = 'ft_trialfun_general';
% cfg.dataset = [folder file format];
% cfg = ft_definetrial(cfg);

% Brainvision segmentation processing
% This is probably the example trialfun that I want to modify to lift
% imagined stimuli eventually
bvs = [];
format = '.vhdr';
bvs.dataset = [folder file format];
bvs.trialfun = 'ft_trialfun_brainhack';
bvs = ft_definetrial(bvs);
data = ft_preprocessing(bvs);

% NExt steps: What does bvs.trl contain really? Need to do more
% preprocessing. How to get imagined stimuli as well?
