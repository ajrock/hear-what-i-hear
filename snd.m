% Clear the workspace

close all
clearvars
sca


%%Prameters
int_iti = [0 0];
sndgain = 1;
pth_stim = ['C:\_User\stimuli'];
addpath(['C:\_User\\trigger']);
addpath([pth_stim,'']);


%pos_screen = [100 100 900 900];
%resol_screen = pos_screen(3:4);
pos_screen = []
resol_screen = [1680 , 1050];
resol_pic = [1024,768];
%scale_pic = min(resol_screen./resol_pic);
scale_pic = 1.37;
resol_sam = [138, 187];
scale_sam = 0.7;

rect_pic(1) = ((resol_screen(1)-scale_pic*resol_pic(1))/2);
rect_pic(2) = ((resol_screen(2)-scale_pic*resol_pic(2))/2);
rect_pic(3) = rect_pic(1)+scale_pic*resol_pic(1);
rect_pic(4) = rect_pic(2)+scale_pic*resol_pic(2);
rect_pic= floor(rect_pic);

%%%%initilize trigger
try
    my_triggerhandler = triggerhandler();
    my_triggerhandler.send_trigger(99);
catch 
    disp(['KANN TRIGGER NICHT SENDEN - MSG:' error.message]);
    sca;
    clear all;
    return;
end;

% Setup PTB with some default values
InitializePsychSound();
PsychDefaultSetup(1);

% Basic screen setup
setup.screenNum  = max(Screen('Screens'));
setup.fullscreen = 1;


%%%%%%init Sound
wavfilename.startle  = 'noiseburst50_000.wav';
[y, freq] = psychwavread(wavfilename.startle);
sound.startle = y'*sndgain;
nrchannels = size(sound.startle,1); % Number of rows == number of channels.
pahandle = PsychPortAudio('Open', [], [], 0, freq, nrchannels);


%%%%%% Define colors
color.white = WhiteIndex(setup.screenNum);
color.grey  = color.white / 2;
color.black = BlackIndex(setup.screenNum);
color.red   = [255 0 0];
color.green = [0 255 0];
color.blue  = [0 0 255];

% Open the screen
if setup.fullscreen ~= 1
    [w,wRect] = Screen('OpenWindow',setup.screenNum,color.grey,pos_screen);
else
    [w,wRect] = Screen('OpenWindow',setup.screenNum,color.grey,[]);
end
Screen('BlendFunction', w, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% Query the frame duration
setup.ifi = Screen('GetFlipInterval', w); 


%%%BACKGROUND
file_pic = sprintf('\\set%d\\bos.jpg',iset);
[pic_background, ~, alpha] = imread([pth_stim,'\startle\',file_pic]);
%        img(:, :, 4) = alpha;
pic_background = Screen('MakeTexture', w, pic_background);

Screen('DrawTexture', w, pic_background);
time.start = Screen('Flip', w);


%%%%%%%%%%%%%%%%% TRIAL LOOP
count_pic = zeros(1,nclass);
class = randperm(nclass);

%%%% Introduction text
Screen('DrawTexture', w, pic_background);

text = ['Startle-Experiment: Dr�cken Sie die Leertaste um fortzufahren'];
[pos.text.x,pos.text.y,pos.text.bbox] = DrawFormattedText(w, text,...
    'center','center',color.black, 80);
Screen('TextSize',w,20);
Screen('TextFont',w,'Arial' );
Screen('Flip',w);
KbStrokeWait;    

for itr = 1:100

        t0 = GetSecs;
      

        %%%%%load & buffer pictures
         Screen('DrawTexture', w, pic_background);
  PsychPortAudio('FillBuffer', pahandle, sound.startle);
        
        %iti
        iti = int_iti(1) + diff(int_iti)*rand(1,1);
        dt = GetSecs-t0;
        if iti-dt>0
            WaitSecs(iti-dt);
        end    
        
        %PRESENT IMG
        time.pic = Screen('Flip', w);
        my_triggerhandler.send_trigger(16*(iclass-1)+ipic);
        

        %%%%%STARTLE
        time.startl = NaN;
        if  ~any([ipic_nostartl,ipic_samstartl]==ipic)
            WaitSecs(dt_startle-(GetSecs-time.pic));
            snd = PsychPortAudio('Start', pahandle, [], 0, 1);
            my_triggerhandler.send_trigger(64);
            
            time.startl  = GetSecs;
        end
        
        %%%%%CLEAR IMAGE
        Screen('DrawTexture', w, pic_background);
        WaitSecs(dur_pic-(GetSecs-time.pic));
        time.end_pic = Screen('Flip', w);
        
        
